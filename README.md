# Aurora

Aurora

This is a project made for automation of the registering process in Faberlic company. Each registered person will start his own ad campaign in Facebook. By looking which registration goes to which campaign this will distribute it accordingly.

<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>